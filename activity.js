/*

	Activity:

	Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
*/


let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
};

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}

function introduce(student){

	const {name,age,classes} = student; //object destructure

	let sentence1 = `Hi! I'm ${name}. I am ${age} years old.`;
	let sentence2 =	`I study the following courses ${classes}`;

	console.log(sentence1);
	console.log(sentence2);

}

introduce(student1);
introduce(student2);


// Cube argument - arrow function
const getCube = (num) => Math.pow(num,3);
let cube = getCube(3);
console.log(cube);

let numArr = [15,16,32,21,21,2];

// Display each item on array - arrow function
/*const displayArr = (array) => array.forEach(function(num){console.log(num)});
displayArr(numArr);*/

const displayArr = (num) => console.log(num);
numArr.forEach(displayArr);


// Square each item on array- arrow function
/*const numSquared = (array) => array.map(function(num){console.log(num**2)});
numSquared(numArr);*/

const getSquare = (num) => num**2;
let numSquared = numArr.map(getSquare);
console.log(numSquared);

/*//
Create a class constructor able to receive 3 arguments
		-It should be able to receive two strings and a number
		-Using the this keyword assign properties:
			name, 
			breed, 
			dogAge = <7x human years> 
			-assign the parameters as values to each property.
*/
class Dog {
	constructor(name,breed,humanYears){
		this.name = name;
		this.breed = breed;
		this.dogAge = humanYears*7;
	}
}

// Create 2 new objects using our constructor. This constructor should be able to create Dog objects.
let dog1 = new Dog("Harleen","Shitzu",3);
let dog2 = new Dog("Quinzel","Shiba Inu",2);

// Log the 2 new Dog objects in the console.
console.log(dog1);
console.log(dog2);